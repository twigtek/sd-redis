#!/usr/bin/env python


import re
import commands


class Redis:
    def __init__(self, agent_config, checks_logger, raw_config):
        self.agent_config = agent_config
        self.checks_logger = checks_logger
        self.raw_config = raw_config

    def run(self):
        stats = {}
        status, out = commands.getstatusoutput(self.redis_info_cmd())
        if status != 0:
            return stats
        # Grab every statistic available and leave it to the end user to
        # determine which fields they care about
        for key, val in [line.split(':') for line in out.splitlines()]:
            stats[key] = val
        return stats

    def redis_info_cmd(self):
        command = self.raw_config.get('Redis', {}).get('client', 'redis-cli')
        password = self.raw_config.get('Redis', {}).get('password', None)
        host = self.raw_config.get('Redis', {}).get('host', '127.0.0.1')
        port = self.raw_config.get('Redis', {}).get('port', 6379)

        if password:
            self.checks_logger.debug('Found Redis password in config')
            return "%s -h %s -p %s -a %s info" % (command, host, port, password)
        else:
            return "%s -h %s -p %s info" % (command, host, port)


if __name__ == '__main__':
    redis = Redis(None, None, {})
    print redis.run()

